﻿using FixtureDiagnostic.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureDiagnostic.model
{
    public class modelMainWindow : INotifyPropertyChanged
    {
        private int _currentPort=0;
        public int CurrentPort
        {
            get { return _currentPort; }
            set
            {
                _currentPort = value;
                NotifyPropertyChanged("CurrentPort");
            }
        }

        private int _fixtureCount;
        public int FixtureCount
        {
            get { return _fixtureCount; }
            set
            {
                _fixtureCount = value;
                
                NotifyPropertyChanged("FixtureCount");
            }
        }

        private int[] _portOnEachFixture ;
        public int[] PortOnEachFixture
        {
            get { return _portOnEachFixture; }
            set
            {
                _portOnEachFixture = value;
            }
        }

        private int _portTestCount;
        public int PortTestCount
        {
            get { return _portTestCount; }
            set
            {
                _portTestCount = value;
                NotifyPropertyChanged("PortTestCount");
            }
        }

        private int _FailedPortCount;
        public int FailedPortCount
        {
            get { return _FailedPortCount; }
            set
            {
                _FailedPortCount = value;
                NotifyPropertyChanged("FailedPortCount");
            }
        }

        private int _lightingPort = 0;
        public int LightingPort
        {
            get { return _lightingPort; }
            set
            {
                _lightingPort = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }
}
