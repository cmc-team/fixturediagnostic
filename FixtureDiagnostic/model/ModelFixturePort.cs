﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureDiagnostic.model
{
    public class ModelFixturePort : INotifyPropertyChanged
    {
        public enum TestResult { None, IsTesting, LEDFailed, Successed, ButtonFailed,LEDButtonFailed}

        

        private TestResult _testResult;
        public TestResult testResult
        {
            get { return _testResult; }
            set
            {
                _testResult = value;
                
                NotifyPropertyChanged("testResult");
            }
        }

        private int _port;
        public int Port
        {
            get { return _port; }
            set
            {
                _port = value;
                NotifyPropertyChanged("Port");
            }
        }

        private bool _isLighting=false;
        public bool isLighting
        {
            get { return _isLighting; }
            set
            {
                _isLighting = value;
                NotifyPropertyChanged("isLighting");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }
}
