﻿using FixtureDiagnostic.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FixtureDiagnostic.MyUserControl;
using FixtureDiagnostic.model;
using System.IO;
using System.Net;
using System.Web;
using Microsoft.Win32;

namespace FixtureDiagnostic
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {                    
        public modelMainWindow _modelMain;
        public UC_fixturePanel _uC_FixturePanel;

        public delegate void MainWindowEvent(int portNum);
        AutoResetEvent _initServiceEvent = new System.Threading.AutoResetEvent(false);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {            
            gd_completePage.button_download.Click += Button_download_Click;            

            _modelMain = new modelMainWindow();            
            _modelMain.PortTestCount= FixtureHelper.getCalibratedPortNum();            
            _modelMain.FixtureCount = FixtureHelper.getFixturesCount();
            //_modelMain.PortOnEachFixture = FixtureHelper.portsOnEachFixture(8);
            _modelMain.PortOnEachFixture = new int[_modelMain.FixtureCount];
            for(int fixture=0; fixture< _modelMain.FixtureCount; fixture++)
            {
                _modelMain.PortOnEachFixture[fixture] = FixtureHelper.portsOnEachFixture(fixture);
            }


            this.DataContext = _modelMain;

            if (_modelMain.PortTestCount > 0)
            {
                setFixturePanel(_modelMain.PortTestCount, _modelMain.PortOnEachFixture);
            }
            setLabelLanguage();            

            SizeToContent = SizeToContent.WidthAndHeight;
            ContentRendered += DisableSizeToContent;

            uc_busyBlocker.Visibility = Visibility.Visible;
            Task.Run(() => waitInitServiceDone());
            Task.Run(() => doInitService());            
            FixtureHelper.onButtonPressedEvent += ControllerAPI_onButtonPressedEvent;            
        }
        private void doInitService()
        {
            FixtureHelper.initService();
            Thread.Sleep(1000);
            _initServiceEvent.Set();
        }

        private void waitInitServiceDone()
        {
            try
            {
                bool waitResult = _initServiceEvent.WaitOne();
                App.LogIt($"waitInitServiceDone: {waitResult}");

                Dispatcher.Invoke(
                new Action(delegate ()
                {
                    try
                    {
                        uc_busyBlocker.Visibility = Visibility.Collapsed;
                        FixtureHelper.startMonitorButton();
                        turnOnNextPortLED();
                    }
                    catch (Exception) { }
                }));
            }
            catch (Exception ex)
            {
                App.LogIt($"waitInitServiceDone: {ex.Message}");
                App.LogIt($"waitInitServiceDone: {ex.StackTrace}");
            }
        }

        private static void DisableSizeToContent(object sender, EventArgs e)
        {
            if (sender is Window win)
            {
                win.SizeToContent = SizeToContent.Manual;
            }
        }

        private void Button_download_Click(object sender, RoutedEventArgs e)
        {
            donwloadReport();
        }

        private void ControllerAPI_onButtonPressedEvent(string port)
        {
            if (_modelMain.CurrentPort.ToString() == port)
            {
                this.Dispatcher.Invoke(
                 new Action(delegate ()
                 {
                     try
                     {
                         if (checkBox_led.IsChecked == true)
                         {
                             checkBox_led.IsChecked = false;
                             DoneFixturePortTest(ModelFixturePort.TestResult.LEDFailed);
                         }
                         else
                         {
                             DoneFixturePortTest(ModelFixturePort.TestResult.Successed);
                         }
                     }
                     catch (Exception) { }
                 }));
            }


        }
        private void setLabelLanguage()
        {
            label_line1.Text = LoadResourceString.Instance.GetStrByResource("Line1");
            label_line2.Text = LoadResourceString.Instance.GetStrByResource("Line2");
            label_line3.Text = LoadResourceString.Instance.GetStrByResource("Line3");
            label_line4.Text = LoadResourceString.Instance.GetStrByResource("Line4");
            label_line5.Text = LoadResourceString.Instance.GetStrByResource("Line5");
            label_line6.Text = LoadResourceString.Instance.GetStrByResource("Line6");
            label_line7.Text = LoadResourceString.Instance.GetStrByResource("Line7");
            label_portText.Content = LoadResourceString.Instance.GetStrByResource("label_portText");
            label_panelIntro.Text = LoadResourceString.Instance.GetStrByResource("label_panelIntro");
            button_Cancel.Content = LoadResourceString.Instance.GetStrByResource("Cancel");
            button_Finish.Content = LoadResourceString.Instance.GetStrByResource("Finish");
            label_ledNotWorking.Content = LoadResourceString.Instance.GetStrByResource("label_ledNotWorking");
            button_portNotWorking.Content = LoadResourceString.Instance.GetStrByResource("button_portNotWorking");
            label_Title.Content = LoadResourceString.Instance.GetStrByResource("Utilities") + " > " + LoadResourceString.Instance.GetStrByResource("FixtureDiagnostics");

            label_portCurrent.FontSize = 40;
        }

        private void setFixturePanel(int portNum,int[] eachPortNum)
        {
            //portNum = 24;
            _uC_FixturePanel = new UC_fixturePanel(portNum, eachPortNum);
            grid_fixturePanel.Children.Add(_uC_FixturePanel);
            _uC_FixturePanel.EventTesting += CutLineEvent;
        }

        private string _cutLinePort = null;
        private async void CutLineEvent(UC_fixturePort uC_FixturePort)
        {
            uC_FixturePort.modelFixturePort.testResult = ModelFixturePort.TestResult.IsTesting;
            uc_busyBlocker.Visibility = Visibility.Visible;
            
            _cutLinePort = uC_FixturePort.modelFixturePort.Port.ToString();

            if(_modelMain.CurrentPort.ToString()!=_cutLinePort)
                await Task.Run(() => switchLight(_modelMain.CurrentPort.ToString(), _cutLinePort));
            else
            {
                int newPortFixtureIndex = FixtureHelper.getFixtureIndex(_modelMain.CurrentPort.ToString());                
                int newPortFixturePortsCount = FixtureHelper.portsOnEachFixture(newPortFixtureIndex);
                if (10 == newPortFixturePortsCount)
                {
                    await Task.Run(() => FixtureHelper.turnOnLedOn10PortFixture(_modelMain.CurrentPort.ToString()));
                }
                else
                {
                    await Task.Run(() => FixtureHelper.turnOnLed(_modelMain.CurrentPort.ToString()));
                }
            }
                
            _modelMain.CurrentPort = uC_FixturePort.modelFixturePort.Port;

            if(gd_completePage.Visibility == Visibility.Visible && gd_processingPage.Visibility == Visibility.Collapsed)
            {
                setProcessingPage();

            }
            
            uc_busyBlocker.Visibility = Visibility.Collapsed;
        }

        private void switchLight(string oldPort,string newPort)
        {            
            int oldPortFixtureIndex = FixtureHelper.getFixtureIndex(oldPort);
            int newPortFixtureIndex = FixtureHelper.getFixtureIndex(newPort);
            int oldPortFixturePortsCount  = FixtureHelper.portsOnEachFixture(oldPortFixtureIndex);
            int newPortFixturePortsCount = FixtureHelper.portsOnEachFixture(newPortFixtureIndex);

            //Same Fixture
            if (oldPortFixtureIndex == newPortFixtureIndex)
            {
                //All on 10 ports fixture
                if (10 == oldPortFixturePortsCount)
                {
                    Task taskTurnOn = new Task(() => FixtureHelper.turnOnLedOn10PortFixture(newPort));
                    taskTurnOn.Start();
                    taskTurnOn.Wait();                                        
                }
                //All on 8 ports fixture
                else
                {
                    Task taskTurnOff = new Task(() => FixtureHelper.turnOffLed(oldPort));
                    taskTurnOff.Start();                    

                    Task taskTurnOn = new Task(() => FixtureHelper.turnOnLed(newPort));
                    taskTurnOn.Start();

                    taskTurnOff.Wait();
                    taskTurnOn.Wait();
                }                
            }
            else // Different Fixture
            {
                // New on 10 ports
                if (10 == newPortFixturePortsCount)
                {
                    Task taskTurnOff = new Task(() => FixtureHelper.turnOffLed(oldPort));
                    taskTurnOff.Start();                    

                    Task taskTurnOn = new Task(() => FixtureHelper.turnOnLedOn10PortFixture(newPort));
                    taskTurnOn.Start();

                    taskTurnOff.Wait();
                    taskTurnOn.Wait();
                }
                //New on 8 ports
                else if (8 == newPortFixturePortsCount)
                {
                    Task taskTurnOff = new Task(() => FixtureHelper.turnOffLed(oldPort));
                    taskTurnOff.Start();                    

                    Task taskTurnOn = new Task(() => FixtureHelper.turnOnLed(newPort));
                    taskTurnOn.Start();

                    taskTurnOff.Wait();
                    taskTurnOn.Wait();
                }
            }           
        }

        private async void turnOnNextPortLED()
        {
            uc_busyBlocker.Visibility = Visibility.Visible;
            
            int portToTurnOn;
            int portArrayLength = _uC_FixturePanel._array_OneRowPorts.Length;
            int portToTurnOff= _modelMain.CurrentPort;
            

            for (int i = 0; i < portArrayLength; i++)
            {
                if (_uC_FixturePanel._array_OneRowPorts[i].modelFixturePort.testResult == ModelFixturePort.TestResult.None)
                {
                    portToTurnOn = i + 1;
                    _modelMain.CurrentPort = portToTurnOn;
                    _uC_FixturePanel._array_OneRowPorts[i].modelFixturePort.testResult = ModelFixturePort.TestResult.IsTesting;
                    //await Task.Run(() => FixtureHelper.turnOnLed(result.ToString()));

                    await Task.Run(() => switchLight(portToTurnOff.ToString(), portToTurnOn.ToString()));
                    _modelMain.CurrentPort = portToTurnOn;
                    uc_busyBlocker.Visibility = Visibility.Collapsed;
                    return;
                }
            }

            await Task.Run(() => FixtureHelper.turnOffLed(_modelMain.CurrentPort.ToString()));
            uc_busyBlocker.Visibility = Visibility.Collapsed;            
            setCompletePage();            
        }       

        private void setCompletePage()
        {
            _modelMain.FailedPortCount = 0;
            for (int i = 0; i < _modelMain.PortTestCount; i++)
            {
                ModelFixturePort.TestResult testResult = _uC_FixturePanel._array_OneRowPorts[i].modelFixturePort.testResult;
                if (testResult == ModelFixturePort.TestResult.LEDFailed || testResult == ModelFixturePort.TestResult.ButtonFailed || testResult == ModelFixturePort.TestResult.LEDButtonFailed)
                {
                    _modelMain.FailedPortCount++;
                }
            }

            gd_processingPage.Visibility = Visibility.Collapsed;
            gd_completePage.Visibility = Visibility.Visible;
            button_Finish.IsEnabled = true;
        }

        private void setProcessingPage()
        {
            gd_processingPage.Visibility = Visibility.Visible;
            gd_completePage.Visibility = Visibility.Collapsed;
            button_Finish.IsEnabled = false;
        }
               
        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            doCloseApp();
        }

        private void button_portNotWorking_Click(object sender, RoutedEventArgs e)
        {
            if (checkBox_led.IsChecked == true)
            {
                DoneFixturePortTest(ModelFixturePort.TestResult.LEDButtonFailed);
            }
            else
            {
                DoneFixturePortTest(ModelFixturePort.TestResult.ButtonFailed);
            }
            checkBox_led.IsChecked = false;
        }

        public void DoneFixturePortTest(ModelFixturePort.TestResult result)
        {
            UC_fixturePort fixturePort;
            fixturePort = _uC_FixturePanel._array_OneRowPorts[_modelMain.CurrentPort - 1];
            fixturePort.modelFixturePort.testResult = result;

            

            uc_busyBlocker.Visibility = Visibility.Visible;
            
            turnOnNextPortLED();
        }

        private void image_close_Click(object sender, RoutedEventArgs e)
        {
            doCloseApp();
        }


        private void image_collapse_Click(object sender, RoutedEventArgs e)
        {
            //string i = Environment.MachineName;
            WindowState = WindowState.Minimized;
        }
     
        private void stopService()
        {
            FixtureHelper.stopMonitorButton();
            FixtureHelper.stopService();            
        }

        private void button_Finish_Click(object sender, RoutedEventArgs e)
        {
            doCloseApp();
        }

        async void doCloseApp()
        {
            uc_busyBlocker.Visibility = Visibility.Visible;
            await Task.Run(() => stopService());
            uc_busyBlocker.Visibility = Visibility.Collapsed;
            App.LogIt($"Start Close");
            this.Close();
        }

        private void donwloadReport()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "FixtureDiagnosticsReport";
            dialog.Filter = "Archive (*.txt)|*.txt";

            var result = dialog.ShowDialog(); //shows save file dialog
            if (result == true)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(dialog.FileName);
                    sw.WriteLine("FutureDial Fixture Diagnostics Report");
                    sw.WriteLine("");
                    sw.WriteLine("Date: " + DateTime.Now.ToString());
                    sw.WriteLine("Workstation: " + Environment.MachineName);
                    sw.WriteLine("");
                    sw.WriteLine(String.Format("Fixtures Tested: {0}", _modelMain.FixtureCount));
                    sw.WriteLine(String.Format("Ports Tested: {0}", _modelMain.PortTestCount));
                    sw.WriteLine(String.Format("Failed Ports: {0}", _modelMain.FailedPortCount));
                    sw.WriteLine("");
                    sw.WriteLine("---------------------");
                    for (int i = 0; i < _uC_FixturePanel._array_OneRowPorts.Length; i++)
                    {
                        UC_fixturePort fixturePort;
                        fixturePort = _uC_FixturePanel._array_OneRowPorts[i];
                        if (fixturePort.modelFixturePort.testResult == ModelFixturePort.TestResult.Successed)
                            sw.WriteLine(String.Format("Port {0}: ", i + 1) + "Success");
                        else if (fixturePort.modelFixturePort.testResult == ModelFixturePort.TestResult.LEDFailed)
                            sw.WriteLine(String.Format("Port {0}: ", i + 1) + "Failure (LED)");
                        else if (fixturePort.modelFixturePort.testResult == ModelFixturePort.TestResult.ButtonFailed)
                            sw.WriteLine(String.Format("Port {0}: ", i + 1) + "Failure (Button)");
                        else if (fixturePort.modelFixturePort.testResult == ModelFixturePort.TestResult.LEDButtonFailed)
                            sw.WriteLine(String.Format("Port {0}: ", i + 1) + "Failure (LED, Button)");
                        else
                            sw.WriteLine(String.Format("Port {0}: ", i + 1) + "");
                    }
                    sw.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }

            }
           
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }
    }
}
