﻿using FixtureDiagnostic.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace FixtureDiagnostic
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {        
        public static void LogIt(string s)
        {
            System.Diagnostics.Trace.WriteLine($"[FixtureDiagnostic] {s}");
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            System.Configuration.Install.InstallContext args = new System.Configuration.Install.InstallContext(null, e.Args);

            if (args.Parameters.ContainsKey("exit"))
            {
                Process[] pMyProcess = Process.GetProcessesByName("FixtureDiagnostic");

                App.LogIt("pMyProcess count" + pMyProcess.Length);

                foreach (Process p in pMyProcess)
                {
                    try
                    {
                        if (p.Id != Process.GetCurrentProcess().Id)
                        {
                            while (!p.HasExited)
                            {
                                try
                                {
                                    p.Kill();
                                }
                                catch (Exception ex)
                                {
                                    App.LogIt("p.Kill " + ex.ToString());
                                }
                                Thread.Sleep(500);
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        App.LogIt("p.Kill " + ex1.ToString());
                    }
                }
                Application.Current.Shutdown();
                return;
            }

            //prevent opening multiple instance of application
            Process process = Process.GetCurrentProcess();
            foreach (Process p in Process.GetProcessesByName(process.ProcessName))
            {
                if (p.Id != process.Id)
                {
                    Application.Current.Shutdown();
                    return;
                }
            }

            ArrayList processList = new ArrayList();
            Process[] pArray = Process.GetProcessesByName("GreenT");
            processList.AddRange(pArray);

            pArray = Process.GetProcessesByName("twLauncher");
            processList.AddRange(pArray);

            App.LogIt("processList Count:" + processList.Count);

            if (processList.Count > 0)
            {
                MessageBox.Show(LoadResourceString.Instance.GetStrByResource("close_CMC_First"));
                Application.Current.Shutdown();
                return;
            }

            if(Process.GetProcessesByName("Calibration").Length > 0)
            {
                MessageBox.Show(LoadResourceString.Instance.GetStrByResource("close_Calibration_First"));
                Application.Current.Shutdown();
                return;
            }

            if (0 == FixtureHelper.getCalibratedPortNum())
            {
                MessageBox.Show(LoadResourceString.Instance.GetStrByResource("do_Calibration_First"));
                Application.Current.Shutdown();
                return;
            }

        }
    }
}
