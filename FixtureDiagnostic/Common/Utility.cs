﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FixtureDiagnostic.Common
{
    public class Utility
    {
        public static string fetchDataFromUrl(string url, string username = "", string password = "")
        {
            string ret = "";
            App.LogIt(string.Format("fetchDataFromUrl: ++ url={0}", url));
            try
            {
                using (WebClient wc = new WebClient())
                {
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    {
                        NetworkCredential nc = new NetworkCredential(username, password);
                        wc.Credentials = nc;
                    }
                    byte[] data = wc.DownloadData(url);
                    ret = Encoding.UTF8.GetString(data);
                }
            }
            catch (Exception e)
            {
                App.LogIt(e.Message);
                App.LogIt(e.StackTrace);
            }
            App.LogIt(string.Format("fetchDataFromUrl: -- ret={0}", ret));
            return ret;
        }

        public static string HttpPostRequest(string url, string postData)
        {
            App.LogIt("HttpPostRequest url:" + url + ", postData:" + postData);

            string response = string.Empty;
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myHttpWebRequest.Method = "POST";
                myHttpWebRequest.ContentType = "application/json";
                byte[] data = Encoding.UTF8.GetBytes(postData);
                myHttpWebRequest.ContentLength = data.Length;

                Stream requestStream = myHttpWebRequest.GetRequestStream();
                requestStream.Write(data, 0, data.Length);
                requestStream.Close();

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                if (HttpStatusCode.OK == myHttpWebResponse.StatusCode)
                {
                    Stream responseStream = myHttpWebResponse.GetResponseStream();
                    StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
                    response = myStreamReader.ReadToEnd();
                    myStreamReader.Close();
                    responseStream.Close();

                }
                myHttpWebResponse.Close();
            }
            catch (Exception ex)
            {
                App.LogIt("HttpPostRequest ex:" + ex.ToString());
            }

            App.LogIt($"HttpPostRequest response:{response}");
            return response;
        }

        public static string GetUniqueFilePath(string fileExtension = "")
        {
            string uniquePath = string.Empty;
            try
            {
                do
                {
                    Guid guid = Guid.NewGuid();
                    string uniqueFileName = guid.ToString();
                    uniquePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), uniqueFileName);
                    if (!string.IsNullOrEmpty(fileExtension))
                    {
                        uniquePath += $".{fileExtension}";
                    }
                } while (File.Exists(uniquePath));
            }
            catch (Exception) { }
            App.LogIt($"GetUniqueFilePath:{uniquePath}");
            return uniquePath;
        }

        public static string ReadAllFileText(string filePath)
        {
            Stream stream = null;
            string ret = string.Empty;
            try
            {
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader sr = new StreamReader(stream))
                {
                    stream = null;
                    ret = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                App.LogIt($"Read {filePath} ex:{ex.ToString()}");
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }

            return ret;
        }
    }
}
