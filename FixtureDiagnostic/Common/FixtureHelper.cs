﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace FixtureDiagnostic.Common
{
    public class FixtureHelper
    {
        public delegate void buttonPressd(string port);
        public static event buttonPressd onButtonPressedEvent;

        // Need to sync following names with LEDConfig.json
        public enum LEDStatus { None, TestingPort}
        public enum LEDStripStatus { None, TestingPort}
        
        private static FileSystemWatcher _watcher = null;
        private static List<int> _fixtureConfig = null;

        static Process _runningArduinoServerProcess = null;

        public class LEDColorConfig
        {
            public string status;
            public string type;
            public string light;
            public string startColor;
            public string endColor;
            public string finalColor;
            public string interval; //second
            public string duration; //second

            public LEDColorConfig()
            {
            }

            public LEDColorConfig(LEDStatus ledStatus)
            {
                status = ledStatus.ToString();
                light = "status";

                switch (ledStatus)
                {
                    case LEDStatus.None:
                        type = "solid";
                        startColor = "000000";
                        endColor = "000000";
                        finalColor = "000000";
                        interval = "0";
                        duration = "0";
                        break;

                    case LEDStatus.TestingPort:
                        type = "solid";
                        startColor = "fdfdfd";
                        endColor = "fdfdfd";
                        finalColor = "fdfdfd";
                        interval = "0";
                        duration = "0";
                        break;
                }
            }

            public LEDColorConfig(LEDStripStatus ledStripStatus)
            {
                status = ledStripStatus.ToString();
                light = "strip";

                switch (ledStripStatus)
                {
                    case LEDStripStatus.None:
                        type = "solid";
                        startColor = "000000";
                        endColor = "000000";
                        finalColor = "000000";
                        interval = "0";
                        duration = "0";
                        break;
                   
                    case LEDStripStatus.TestingPort:
                        type = "solid";
                        startColor = "fdfdfd";
                        endColor = "fdfdfd";
                        finalColor = "fdfdfd";
                        interval = "0";
                        duration = "0";
                        break;
                }
            }
        }

        public class LEDConfig
        {
            public List<LEDColorConfig> LEDStatusConfig;
            public List<LEDColorConfig> LEDStripConfig;
        }

        public class Serialconfig
        {
            public List<object> portlabel;
            public List<object> serialports;
        }

        private static LEDConfig _LEDConfig = null;

        public static void initService()
        {
            string exe = Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "arduinoServer.exe");
            if (File.Exists(exe))
            {                
                try
                {
                    _runningArduinoServerProcess = new Process();
                    _runningArduinoServerProcess.StartInfo.FileName = exe;
                    _runningArduinoServerProcess.StartInfo.Arguments = "-start-service";
                    _runningArduinoServerProcess.StartInfo.CreateNoWindow = true;
                    _runningArduinoServerProcess.StartInfo.UseShellExecute = false;
                    _runningArduinoServerProcess.StartInfo.RedirectStandardOutput = true;
                    _runningArduinoServerProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    _runningArduinoServerProcess.EnableRaisingEvents = true;
                    _runningArduinoServerProcess.Start();                    
                }
                catch (Exception ex)
                {
                    App.LogIt($"initService ex:{ex.ToString()}");
                }
            }

            string response = string.Empty;
            DateTime startTime = DateTime.Now;

            do
            {
                response = Utility.fetchDataFromUrl("http://localhost:3420/serialstatus");
            } while (string.IsNullOrEmpty(response) && DateTime.Now.Subtract(startTime).TotalSeconds < 90);
        }

        public static void stopService()
        {
            App.LogIt($"stopService ++");

            string exeDeviceFixtureCtrl = Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "deviceFixtureCtrl.exe");
            if (File.Exists(exeDeviceFixtureCtrl))
            {
                try
                {
                    Process p = new Process();
                    p.StartInfo.FileName = exeDeviceFixtureCtrl;
                    p.StartInfo.Arguments = "-clearLed -label=1";
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.EnableRaisingEvents = true;
                    p.Start();
                    p.WaitForExit(10 * 1000);
                    App.LogIt($"deviceFixtureCtrl exit");
                }
                catch (Exception ex)
                {
                    App.LogIt($"stopService ex:{ex.ToString()}");
                }
            }

            string exe = Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "arduinoServer.exe");
            if (File.Exists(exe))
            {
                try
                {
                    Process p = new Process();
                    p.StartInfo.FileName = exe;
                    p.StartInfo.Arguments = "-kill-service";
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.EnableRaisingEvents = true;
                    p.Start();
                    p.WaitForExit(10 * 1000);


                    if(null != _runningArduinoServerProcess && !_runningArduinoServerProcess.HasExited)
                    {
                        _runningArduinoServerProcess.WaitForExit(30 * 1000);
                    }

                    Thread.Sleep(1000);
                    App.LogIt($"arduinoServer exit");
                }
                catch (Exception ex)
                {
                    App.LogIt($"stopService ex:{ex.ToString()}");
                }
            }

            App.LogIt($"stopService --");
        }


        public static void startMonitorButton()
        {
            string exe = Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "deviceFixtureCtrl.exe");
            if (File.Exists(exe))
            {
                string file = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "fixtureButton.xml");
                try
                {
                    WathFileChage(file);

                    Process p = new Process();
                    p.StartInfo.FileName = exe;
                    p.StartInfo.Arguments = $"-startMonitorButton -label=1 -file=\"{file}\"";
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.EnableRaisingEvents = true;
                    p.Start();
                }
                catch (Exception ex)
                {
                    App.LogIt($"startMonitorButton ex:{ex.ToString()}");
                }
            }
        }

        public static void stopMonitorButton()
        {
            if (null != _watcher)
            {
                _watcher.EnableRaisingEvents = false;
                _watcher.Dispose();
            }

            string exe = Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "deviceFixtureCtrl.exe");
            if (File.Exists(exe))
            {
                try
                {
                    Process p = new Process();
                    p.StartInfo.FileName = exe;
                    p.StartInfo.Arguments = "-stopMonitorButton -label=1";
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.EnableRaisingEvents = true;
                    p.Start();
                    p.WaitForExit(5 * 1000);
                }
                catch (Exception ex)
                {
                    App.LogIt($"startMonitorButton ex:{ex.ToString()}");
                }
            }
        }

        public static void setLEDStatus(string port, LEDStatus status)
        {
            App.LogIt($"setLEDStatus port:{port} status:{status}");
            
            LEDColorConfig colorConfig = GetLEDStatusColorConfig(status);
            doSetLED(port, colorConfig);
        }

        public static void setLEDStripStatus(string port, LEDStripStatus status)
        {
            App.LogIt($"setLEDStripStatus port:{port} status:{status}");
            
            LEDColorConfig colorConfig = GetLEDStripColorConfig(status);
            doSetLED(port, colorConfig);
        }

        private static LEDColorConfig GetLEDStatusColorConfig(LEDStatus status)
        {
            LEDColorConfig ret = null;
            if (null != _LEDConfig && null != _LEDConfig.LEDStatusConfig)
            {
                ret = _LEDConfig.LEDStatusConfig.Find(p => p.status.Equals(status.ToString()));
            }

            if (null == ret)
            {
                ret = new LEDColorConfig(status);
            }

            return ret;
        }

        private static LEDColorConfig GetLEDStripColorConfig(LEDStripStatus stripStatus)
        {
            LEDColorConfig ret = null;
            if (null != _LEDConfig && null != _LEDConfig.LEDStripConfig)
            {
                ret = _LEDConfig.LEDStripConfig.Find(p => p.status.Equals(stripStatus.ToString()));
            }

            if (null == ret)
            {
                ret = new LEDColorConfig(stripStatus);
            }
            return ret;
        }

        private static void doSetLED(string port, LEDColorConfig colorConfig)
        {
            string exe = Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "deviceFixtureCtrl.exe");
            if (File.Exists(exe) && null != colorConfig)
            {
                if (colorConfig.light.Equals("status") || colorConfig.light.Equals("strip"))
                {
                    try
                    {
                        Process p = new Process();
                        p.StartInfo.FileName = exe;

                        if (colorConfig.type.Equals("blink"))
                        {
                            float interval = 0.5f;
                            float.TryParse(colorConfig.interval, out interval);

                            int duration = 0;
                            Int32.TryParse(colorConfig.duration, out duration);

                            p.StartInfo.Arguments = $"-setLed -label={port} light={colorConfig.light} type=blink finalColor={colorConfig.finalColor} startColor={colorConfig.startColor} endColor={colorConfig.endColor} " +
                            $"interval={interval} -kill";
                            if (duration > 0)
                                p.StartInfo.Arguments += $" duration={duration}";
                        }
                        else
                        {
                            p.StartInfo.Arguments = $"-setLed -label={port} light={colorConfig.light} type=solid finalColor={colorConfig.finalColor} -kill";
                        }

                        p.StartInfo.CreateNoWindow = true;
                        p.StartInfo.UseShellExecute = false;
                        p.StartInfo.RedirectStandardOutput = true;
                        p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        p.EnableRaisingEvents = true;
                        p.Start();

                        if (colorConfig.type.Equals("solid"))
                        {
                            p.WaitForExit(1 * 1000);
                        }
                    }
                    catch (Exception ex)
                    {
                        App.LogIt($"setLED ex:{ex.ToString()}");
                    }
                }
            }
        }

        private static void WathFileChage(string xmlPath)
        {
            App.LogIt(string.Format("WathFileChage: ++ {0}", xmlPath));

            if (string.IsNullOrEmpty(xmlPath))
                return;

            if (null != _watcher)
            {
                _watcher.EnableRaisingEvents = false;
                _watcher.Dispose();
            }

            _watcher = new FileSystemWatcher();
            _watcher.Path = Path.GetDirectoryName(xmlPath);
            _watcher.NotifyFilter = NotifyFilters.LastWrite;
            _watcher.Filter = Path.GetFileName(xmlPath);
            _watcher.Changed += new FileSystemEventHandler(OnChanged);
            _watcher.EnableRaisingEvents = true;

        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            App.LogIt(string.Format("OnChanged: ++ {0}", e.FullPath));

            string text = Utility.ReadAllFileText(e.FullPath);

            if (!string.IsNullOrEmpty(text))
            {
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(text);
                    XmlNode node = xmlDoc.SelectSingleNode("labelinfo/runtime/fixtureButton");
                    if (null != node)
                    {
                        onButtonPressedEvent?.Invoke(node.InnerText.Trim());
                    }
                }
                catch (Exception ex)
                {
                    App.LogIt($"OnChanged ex:{ex.ToString()}");
                }
            }
        }

        public static void turnOnLed(string port)
        {
            if ("0" != port)
            {
                setLEDStatus(port, LEDStatus.TestingPort);
                setLEDStripStatus(port, LEDStripStatus.TestingPort);
            }
        }

        public static void turnOffLed(string port)
        {
            if ("0" != port)
            {
                setLEDStatus(port, LEDStatus.None);
                setLEDStripStatus(port, LEDStripStatus.None);
            }  
        }

        public static void turnOnLedOn10PortFixture(string port)
        {
            if ("0" != port)
            {
                try
                {                   
                    int intPort = Int32.Parse(port);                                     
                    JTokenWriter writer = new JTokenWriter();
                    writer.WriteStartObject();
                    writer.WritePropertyName("status");
                    writer.WriteValue("test");
                    writer.WritePropertyName("label");
                    writer.WriteValue(intPort);
                    writer.WritePropertyName("colors");
                    writer.WriteStartArray();
                    writer.WriteStartArray();
                    writer.WriteValue(255);
                    writer.WriteValue(255);
                    writer.WriteValue(255);
                    writer.WriteEndArray();
                    writer.WriteEndArray();
                    writer.WriteEndObject();
                    JObject rss = (JObject)writer.Token;                    
                    string jsonData = rss.ToString();
                    Utility.HttpPostRequest("http://localhost:3420/leds", jsonData);
                }
                catch (Exception ex) {
                    App.LogIt($"turnOnLedOn10PortFixture port:{port}, ex:{ex.ToString()}");
                }                
            }
        }

        public static int getCalibratedPortNum()
        {
            initFixtureConfig();
            return null != _fixtureConfig ? _fixtureConfig.Sum() : 0;
        }

        public static int getFixturesCount()
        {
            initFixtureConfig();
            return null == _fixtureConfig ? 0 : _fixtureConfig.Count;
        }

        public static int portsOnEachFixture(int fixtureIndex)
        {
            int ret = 0;

            initFixtureConfig();

            if(null != _fixtureConfig && _fixtureConfig.Count > fixtureIndex && fixtureIndex >= 0 )
            {
                ret = _fixtureConfig[fixtureIndex];
            }

            return ret;
        }

        public static int getFixtureIndex(string calibratedPortNum)
        {
            int index = 0;
            int portNum;
            int totalPorts = 0;

            try
            {
                portNum = Int32.Parse(calibratedPortNum);
            }
            catch (Exception ex) {
                App.LogIt($"getFixtureIndex ex:{ex.ToString()}");
                return -1;
            }

            if(0 == portNum)
            {
                return -1;
            }

            initFixtureConfig();

            if (null != _fixtureConfig)
            {
                for(index = 0; index < _fixtureConfig.Count; index++)
                {
                    totalPorts += _fixtureConfig[index];
                    if (totalPorts >= portNum)
                    {
                        return index;
                    }
                }
            }

            return index;
        }

        private static void initFixtureConfig()
        {
            if (null == _fixtureConfig)
            {
                string config = System.IO.Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "Serialconfig.json");

                if (File.Exists(config))
                {
                    try
                    {
                        IDictionary<string, JToken> jsonData = JObject.Parse(File.ReadAllText(config));

                        int nFixtureIndex = 0;

                        while (jsonData.ContainsKey(nFixtureIndex.ToString()) && nFixtureIndex < jsonData.Count)
                        {
                            JToken fixtureElement = jsonData[nFixtureIndex.ToString()];
                            if (fixtureElement is JObject)
                            {
                                Dictionary<string, object> dict = fixtureElement.ToObject<Dictionary<string, object>>();
                                if (dict.ContainsKey("portlabel") && dict["portlabel"] is JArray)
                                {
                                    if (null == _fixtureConfig)
                                    {
                                        _fixtureConfig = new List<int>();
                                    }
                                    _fixtureConfig.Add(((JArray)dict["portlabel"]).Count);
                                }
                            }
                            nFixtureIndex++;
                        }
                    }
                    catch (Exception ex)
                    {
                        App.LogIt($"getFixturesCount ex:{ex.ToString()}");
                        _fixtureConfig = null;
                    }
                }
            }
        }
    }
}
