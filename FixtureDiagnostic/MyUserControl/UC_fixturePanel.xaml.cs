﻿using FixtureDiagnostic.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FixtureDiagnostic.Common;

namespace FixtureDiagnostic.MyUserControl
{
    /// <summary>
    /// UC_fixturePanel.xaml 的互動邏輯
    /// </summary>
    public partial class UC_fixturePanel : UserControl
    {
        public delegate void Event(UC_fixturePort uC_FixturePort);
        public event Event EventTesting;
        public UC_fixturePort[,] _array_FixturePorts;
        public UC_fixturePort[] _array_OneRowPorts;
        public int _FixtureNumber;
        public int[] _portNumInEachFixture;
        public int _totalPortNum;
        

        public UC_fixturePanel(int totalPortNum,int[] portNumInEachFixture)
        {
            InitializeComponent();
            _totalPortNum = totalPortNum;
            _portNumInEachFixture = portNumInEachFixture;
            
            setFixturePanel(_totalPortNum, _portNumInEachFixture);                     
        }       


        private void doPortTest(int port)
        {
            for(int i =0;i<_totalPortNum; i++)
            {
                if(i+1==port)
                {
                    //_array_OneRowPorts[i].modelFixturePort.testResult = ModelFixturePort.TestResult.IsTesting;
                    EventTesting?.Invoke(_array_OneRowPorts[i]);
                }
                else
                {
                    if(_array_OneRowPorts[i].modelFixturePort.testResult==ModelFixturePort.TestResult.IsTesting)
                    {
                        _array_OneRowPorts[i].modelFixturePort.testResult = ModelFixturePort.TestResult.None;
                    }
                }
            }
        }

        private void setFixturePanel(int portNum, int[] portEachFixture)
        {
            _FixtureNumber = portEachFixture.Length;
            if(portEachFixture.Length>0)
            {
                int portSerial = 1;
                _array_OneRowPorts = new UC_fixturePort[portNum];
                for (int fixture = 0; fixture < portEachFixture.Length; fixture++)
                {
                    StackPanel grid_fixture = new StackPanel();
                    Thickness margin  = grid_fixture.Margin;
                    margin.Bottom = 5;
                    grid_fixture.Margin = margin;
                    grid_fixture.Orientation = Orientation.Horizontal;
                    grid_fixture.Tag = fixture;

                    for (int port = 0; port < portEachFixture[fixture]; port++) 
                    {                        
                        UC_fixturePort uC_FixturePort = new UC_fixturePort();
                        uC_FixturePort.clickedPort += UC_FixturePort_clickedPort;
                        uC_FixturePort.modelFixturePort.Port = portSerial;
                        grid_fixture.Children.Add(uC_FixturePort);
                        _array_OneRowPorts[portSerial-1] = uC_FixturePort;
                        portSerial++;
                    }
                    grid_layout.Children.Add(grid_fixture);
                }
            }
        }

        
        private void UC_FixturePort_clickedPort(model.ModelFixturePort modelFixturePort)
        {
            doPortTest(modelFixturePort.Port);
        }

        

        
    }
}
