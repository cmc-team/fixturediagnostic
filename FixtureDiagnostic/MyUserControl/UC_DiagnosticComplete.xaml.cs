﻿using FixtureDiagnostic.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FixtureDiagnostic.MyUserControl
{
    /// <summary>
    /// UC_DiagnosticComplete.xaml 的互動邏輯
    /// </summary>
    public partial class UC_DiagnosticComplete : UserControl
    {
        public UC_DiagnosticComplete()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            label_ReportSummary.Content = LoadResourceString.Instance.GetStrByResource("ReportSummary");
            label_FixtureTested.Content = LoadResourceString.Instance.GetStrByResource("FixturesTested:");
            label_PortTested.Content = LoadResourceString.Instance.GetStrByResource("PortsTested:");
            label_Failedport.Content = LoadResourceString.Instance.GetStrByResource("FailedPorts:");
            button_download.Content= LoadResourceString.Instance.GetStrByResource("DownloadDiagnosticReport");
        }        
    }
}
