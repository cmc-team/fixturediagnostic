﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FixtureDiagnostic.MyUserControl
{
    /// <summary>
    /// UC_busyBlocker.xaml 的互動邏輯
    /// </summary>
    public partial class UC_busyBlocker : UserControl
    {
        public UC_busyBlocker()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Animation_busyBlock);
        }

        void Animation_busyBlock(object sender, RoutedEventArgs e)
        {
            RotateTransform rtf = new RotateTransform();
            rtf.CenterX = Img_BusyBlock.Width / 2;
            rtf.CenterY = Img_BusyBlock.Height / 2;
            Img_BusyBlock.RenderTransform = rtf;
            DoubleAnimation dbAscending = new DoubleAnimation(0, 360, new Duration(TimeSpan.FromSeconds(3)));
            dbAscending.RepeatBehavior = RepeatBehavior.Forever;
            rtf.BeginAnimation(RotateTransform.AngleProperty, dbAscending);
        }
    }
}
