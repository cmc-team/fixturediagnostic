﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FixtureDiagnostic.model;


namespace FixtureDiagnostic.MyUserControl
{
    /// <summary>
    /// UC_fixturePort.xaml 的互動邏輯
    /// </summary>
    public partial class UC_fixturePort : UserControl
    {
        public ModelFixturePort modelFixturePort;
        public delegate void somethingClickEvent(ModelFixturePort modelFixturePort);
        public event somethingClickEvent clickedPort;

        public UC_fixturePort()
        {
            InitializeComponent();
            modelFixturePort = new ModelFixturePort();
            this.DataContext = modelFixturePort;
        }


        private void button_port_Click(object sender, RoutedEventArgs e)
        {
            clickedPort?.Invoke(modelFixturePort);
        }
    }
}
